package utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

import java.util.List;

/**
 * Created by tringuyen on 3/2/17.
 */
public class ElementActions
{
    public static WebDriver driver;

    public void mouseHover(WebDriver wDriver, WebElement wElement)
    {
        Actions action = new Actions(wDriver);
        action.moveToElement(wElement).build().perform();
    }

    public void selectDropdownElement(WebElement wElement, String myOption)
    {
        WebElement mySelectElement = wElement;
        Select mySelect= new Select(mySelectElement);
//        List<WebElement> options= mySelect.getOptions();
//        for(WebElement option:options)
//        {
//            if(option.getText().equalsIgnoreCase(myOption))
//                option.click();
//        }
        mySelect.selectByVisibleText(myOption);
    }

}

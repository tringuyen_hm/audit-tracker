package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;
/**
 * Created by tringuyen on 2/28/17.
 */
public class MenuLinkPage
{
    ElementActions eActions =new ElementActions();
    public static WebDriver driver;

    @FindBy(xpath = "//a[contains(text(),'Sumo')]")
    public static WebElement sumoMenu;

    @FindBy(xpath = "//a[contains(text(),'Smelters')]")
    public static WebElement smeltersSubMenu;

    @FindBy(xpath = "//a[contains(text(),'Audits')]")
    public static WebElement auditSubMenu;

    public MenuLinkPage(WebDriver wDriver)
    {
        driver=wDriver;
        PageFactory.initElements(driver,this);
    }

//    public void mouseHover(WebDriver wDriver, WebElement wElement)
//    {
//        Actions action = new Actions(wDriver);
//        action.moveToElement(wElement).build().perform();
//    }

    public void goToSmelters()
    {
        eActions.mouseHover(driver,sumoMenu);
        smeltersSubMenu.click();
    }

    public void goToAudits()
    {
        eActions.mouseHover(driver,auditSubMenu);
    }
}

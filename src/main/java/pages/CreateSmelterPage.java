package pages;

import com.github.javafaker.Faker;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.*;
import java.util.Random;

/**
 * Created by tringuyen on 2/28/17.
 */
public class CreateSmelterPage
{
    public static WebDriver driver;
    ElementActions eActions = new ElementActions();

    @FindBy(css = ".title-label")
    public static WebElement pageTitle;

    @FindBy(xpath = "//input[@name='cid']")
    public static WebElement cidInput;

    @FindBy(xpath ="//input[@name='smelterName']")
    public static WebElement smelterNameInput;

    @FindBy(xpath = "//input[@name='address1']")
    public static WebElement address1Input;

    @FindBy(xpath = "//input[@name='address2']")
    public static WebElement address2Input;

    @FindBy(xpath = "//input[@name='city']")
    public static WebElement cityInput;

    @FindBy(xpath = "//select[@name='countryCode']")
    public static WebElement countryCodeSelect;

    @FindBy(xpath = "//input[@name='postCode']")
    public static WebElement postCodeInput;

    @FindBy(xpath = "//input[@name='region']")
    public static WebElement regionInput;

    @FindBy(xpath = "//select[@name='groupId']")
    public static WebElement groupIdSelect;

    @FindBy(xpath = "//select[@name='metalId']")
    public static WebElement metalIdSelect;

    @FindBy(xpath = "//button[contains(text(),'Cancel')]")
    public static WebElement cancelButton;

    @FindBy(xpath = "//button[contains(text(),'Clear')]")
    public static WebElement clearButton;

    @FindBy(xpath = "//button[contains(text(),'Submit')]")
    public static WebElement submitButton;

    public String smelterName;

    public CreateSmelterPage(WebDriver wDriver)
    {
        driver=wDriver;

        PageFactory.initElements(driver,this);
    }

    public void clearAllField()
    {
        cidInput.clear();
        smelterNameInput.clear();
        address1Input.clear();
        address2Input.clear();
        cityInput.clear();
        eActions.selectDropdownElement(countryCodeSelect,"Please Select");
        postCodeInput.clear();
        regionInput.clear();
        eActions.selectDropdownElement(groupIdSelect,"Please Select");
        eActions.selectDropdownElement(metalIdSelect,"Please Select");

    }

    public void fillingUpData()
    {
        Faker faker = new Faker();
        cidInput.sendKeys("CID00" + getRandomNumberInRange(1000,9999));
        smelterName=faker.name().fullName();
        smelterNameInput.sendKeys(smelterName);
        address1Input.sendKeys(faker.address().streetAddressNumber());
        address2Input.sendKeys(faker.address().streetAddressNumber());
        cityInput.sendKeys(faker.address().city());
        eActions.selectDropdownElement(countryCodeSelect,"Viet Nam");
        postCodeInput.sendKeys(faker.address().zipCode());
        regionInput.sendKeys(faker.address().country());
        eActions.selectDropdownElement(groupIdSelect,"KEMET Corp.");
        eActions.selectDropdownElement(metalIdSelect,"Gold");
    }

    public void submitCreateSmelter(WebDriver wDriver)
    {
        submitButton.click();
    }

    public void clearFieldCreateSmelter(WebDriver wDriver)
    {
       clearButton.click();
    }

    public void cancelCreateSmelter(WebDriver wDriver)
    {
        cancelButton.click();
    }



    public static int getRandomNumberInRange(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("Max must be greater than Min.");
        }
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }
}


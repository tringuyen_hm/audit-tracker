package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utils.ElementActions;


/**
 * Created by tringuyen on 2/28/17.
 */
public class SmeltersPage
{
    public static WebDriver driver;
    ElementActions eActions=new ElementActions();

    @FindBy(xpath = "//button[contains(text(),'New Smelter')]")
    public static WebElement newSmelterButton;

    @FindBy(xpath = "//button[contains(text(),'Manage Audits')]")
    public static WebElement manageAuditsButton;

    @FindBy(xpath = "//button[contains(text(),'Apply Filters')]")
    public static WebElement applyFiltersButton;

    @FindBy(xpath = "//input[@name='smelterName']")
    public static WebElement smelterNameInput;

    @FindBy(xpath = "//label[contains(text(),'Country')]/following-sibling::div/select")
    public static WebElement countrySelect;

    @FindBy(xpath = "//label[contains(text(),'Group Name')]/following-sibling::div/select")
    public static WebElement groupNameSelect;

    @FindBy(xpath = "//label[contains(text(),'Metal')]/following-sibling::div/select")
    public static WebElement metalSelect;

    @FindBy(xpath = "//div[@class='ag-root ag-font-style ag-scrolls']")
    public static WebElement smelterTable;

    @FindBy(id="btFirst")
    public static WebElement firstButton;

    @FindBy(id="btPrevious")
    public static WebElement previousButton;

    @FindBy(id="current")
    public static WebElement currentPage;

    @FindBy(id="total")
    public static WebElement totalPage;

    @FindBy(id = "btNext")
    public static WebElement nextPage;

    @FindBy(id="btLast")
    public static WebElement lastPage;

    public SmeltersPage(WebDriver wDriver)
    {
        driver=wDriver;
        PageFactory.initElements(driver,this);
    }

    public void goToCreateNewSmelter()
    {
        newSmelterButton.click();
    }

    public void filterByConditions( String smelterName, String country, String groupName, String metal)
    {
        smelterNameInput.sendKeys(smelterName);
        eActions.selectDropdownElement(countrySelect,country);
        eActions.selectDropdownElement(groupNameSelect,groupName);
        eActions.selectDropdownElement(metalSelect,metal);
        applyFiltersButton.click();
    }

}

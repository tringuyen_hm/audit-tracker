package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by tringuyen on 2/25/17.
 */
public class LoginPage
{
    @FindBy(id="username")
    public static WebElement emailInput;

    @FindBy(id="password")
    public static WebElement passwordInput;

    @FindBy(id="loginBtn")
    public static WebElement loginButton;

    @FindBy(id="passwordForgottenLink")
    public static WebElement passwordForgottenLink;

    WebDriver driver;
    public LoginPage(WebDriver wDriver)
    {
        this.driver =wDriver;
        PageFactory.initElements(driver,this);
    }

    public void loginWithEmailAndPassword(String email, String password)
    {
        emailInput.clear();
        emailInput.sendKeys(email);
        passwordInput.clear();
        passwordInput.sendKeys(password);
        loginButton.click();
    }
}

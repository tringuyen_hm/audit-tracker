package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by tringuyen on 2/25/17.
 */
public class WelcomePage
{
    @FindBy(id="mainTitleSSO")
    public static WebElement pageTitle;

    @FindBy(id="logInLink")
    public static WebElement loginButton;

    public static WebDriver driver;

    public WelcomePage(WebDriver wDriver)
    {
        driver = wDriver;
        PageFactory.initElements(driver,this);
    }

    public void goToLoginPage()
    {
        loginButton.click();
    }
}

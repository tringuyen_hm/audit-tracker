package com;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.LoginPage;
import pages.WelcomePage;
import utils.Constants;

import java.util.concurrent.TimeUnit;

/**
 * Created by tringuyen on 2/25/17.
 */
public class LoginTest extends BaseTest
{
//    public  static WebDriver driver;
    private WelcomePage welcomePage;
    private LoginPage loginPage;

   @BeforeClass
    public void initTest()
   {
       welcomePage=new WelcomePage(driver);
       loginPage=new LoginPage(driver);
   }


    @Test
    public void LoginSumoSuccess()
    {
        welcomePage.goToLoginPage();
        loginPage.loginWithEmailAndPassword(Constants.EMAIL,Constants.PASSWORD);
    }

}
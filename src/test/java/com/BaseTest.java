package com;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import pages.LoginPage;
import pages.WelcomePage;
import utils.Constants;

import java.util.concurrent.TimeUnit;

/**
 * Created by tringuyen on 2/27/17.
 */
public class BaseTest
{
    public static WebDriver driver;

    @BeforeClass
    public void init()
    {
        if(OSDetector()=="Mac")
        {
            System.setProperty("webdriver.chrome.driver", "src//test//java//resources//chromedriver");
        }
        else
            if(OSDetector()=="Windows")
            {
                System.setProperty("webdriver.chrome.driver", "src//test//java//resources//chromedriver.exe");
            }
        driver=new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        deleteAllCookies();
        maximizeWindows();
        driver.get(Constants.URL);
    }

    public void  deleteAllCookies()
    {
        driver.manage().deleteAllCookies();
    }

    public void maximizeWindows()
    {
        driver.manage().window().maximize();
    }

    @AfterClass
    public void cleanUp()
    {
        if(driver!=null)
        {
            driver.quit();
        }
    }

    private  String OSDetector () {
        String os = System.getProperty("os.name").toLowerCase();
        if (os.contains("win")) {
            return "Windows";
        } else if (os.contains("nux") || os.contains("nix")) {
            return "Linux";
        }else if (os.contains("mac")) {
            return "Mac";
        }else if (os.contains("sunos")) {
            return "Solaris";
        }else {
            return "Other";
        }
    }
}

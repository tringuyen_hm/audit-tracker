package com;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import pages.*;
import utils.Constants;

/**
 * Created by tringuyen on 3/2/17.
 */
public class CreateSmelterTest extends BaseTest
{
    private WelcomePage welcomePage;
    private LoginPage loginPage;
    private CreateSmelterPage createSmelterPage;
    private MenuLinkPage menuLinkPage;
    private SmeltersPage smeltersPage;

    @BeforeClass
    public void initTest()
    {
        welcomePage=new WelcomePage(driver);
        welcomePage.goToLoginPage();
        loginPage=new LoginPage(driver);
        menuLinkPage=new MenuLinkPage(driver);
        smeltersPage=new SmeltersPage(driver);
        createSmelterPage=new CreateSmelterPage(driver);

    }

    @Test
    public void CreateNewSmelterTest() throws InterruptedException
    {
        loginPage.loginWithEmailAndPassword(Constants.EMAIL, Constants.PASSWORD);
        menuLinkPage.goToSmelters();
        smeltersPage.goToCreateNewSmelter();
        createSmelterPage.clearAllField();
        createSmelterPage.fillingUpData();
        Thread.sleep(3000);
        createSmelterPage.submitCreateSmelter(driver);
        Thread.sleep(3000);
        menuLinkPage.goToSmelters();
        smeltersPage.filterByConditions(createSmelterPage.smelterName,"Please Select","Please Select","Please Select");
    }

}
